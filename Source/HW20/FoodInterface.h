// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FoodInterface.generated.h"

class ASnakeBase;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFoodInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class HW20_API IFoodInterface
{
	GENERATED_BODY()

public:
	UFUNCTION()
	virtual void WasEaten(AActor* Interactor, bool bIsHead);

	UFUNCTION()
	virtual void Reaction(ASnakeBase* Snake);



	// Add interface functions to this class. This is the class that will be inherited to implement this interface.

};
