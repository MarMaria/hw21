// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "SnakeBase.h"

// Sets default values
AObstacle::AObstacle()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(0.5f);

	FVector temp;
	GetActorBounds(false, temp, Size);

	delta = 10.f;
	MaxHeight = Size.Z;
	MinHeight = -MaxHeight;
}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsTick)
	{
		Move();
	}
}

void AObstacle::Interact(AActor * Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

void AObstacle::Move()
{
	if (GetActorLocation().Z > MaxHeight)
	{
		delta = -10.f;
	}
	else if (GetActorLocation().Z < MinHeight)
	{
		delta = 10.f;
	}

	FVector DeltaLocation(0, 0, delta);
	SetActorLocation(GetActorLocation() + DeltaLocation);
}


