// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodInterface.h"
#include "SnakeBase.h"

// Add default functionality here for any IFoodInterface functions that are not pure virtual.

void IFoodInterface::WasEaten(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Reaction(Snake);
		}
	}
}

void IFoodInterface::Reaction(ASnakeBase* Snake)
{
}
