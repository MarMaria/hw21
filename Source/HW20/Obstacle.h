// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Obstacle.generated.h"

UCLASS()
class HW20_API AObstacle : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacle();

	UPROPERTY(EditAnywhere)
	bool bIsTick = true;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float delta;
	float MaxHeight;
	float MinHeight;
	FVector Size;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void Move();

};
