// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "Poison.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);

	FVector NewLocation(0, 0, 0);
	FTransform NewTransform(NewLocation);
	ASnakeElementBase* TempSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	FVector temp;
	TempSnakeElem->GetActorBounds(false, temp, ElementBounds);
	ElementSize = 2 * ElementBounds.X + 1;
	TempSnakeElem->Destroy();

	AddSnakeElement(5);

	NewFood();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	FVector lastLocation(0, 0, 0);
	bool FirstSpawn = true;
	if (SnakeElements.Num() != 0)
	{
		lastLocation = SnakeElements.Last()->GetActorLocation();
		FirstSpawn = false;
	}

	for (int i = 0; i < ElementsNum; ++i)
	{	
		FVector NewLocation(SnakeElements.Num() * ElementSize * (int)FirstSpawn + lastLocation.X,
			lastLocation.Y, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::DeleteSnakeElement(int ElementsNum)
{
	for (int i = ElementsNum; i > 0; i--)
	{
		SnakeElements[SnakeElements.Num() - i]->Destroy();
		SnakeElements.RemoveAt(SnakeElements.Num() - i);
	}

	if (SnakeElements.Num() < MinSize)
	{
		Destroy();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector = FVector(0, 0, 0);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractbleInterface = Cast<IInteractable>(Other);

		if (InteractbleInterface)
		{
			InteractbleInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::NewFood()
{
	//TODO: Random �������
	int32 randX = FMath::RandRange(-6, 7);
	int32 randY = FMath::RandRange(-7, 7);

	FVector FoodLocation(randX * 10.f - 5, randY * 10.f, 0);
	FTransform FoodTransform(FoodLocation);

	int32 randType = FMath::RandRange(0, 10);

	if (randType < 5)
	{
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);
	}
	else if (randType < 8)
	{
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(DoubleFoodClass, FoodTransform);
	}
	else
	{
		int32 randXPoison = FMath::RandRange(-6, 7);
		int32 randYPoison = FMath::RandRange(-7, 7);

		FVector PoisonLocation(randXPoison * 10.f - 5, randYPoison * 10.f, 0);
		FTransform PoisonTransform(PoisonLocation);

		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);
		APoison* NewPoison = GetWorld()->SpawnActor<APoison>(PoisonClass, PoisonTransform);
	}

	
}

